const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')

const app = express()
const port = 3001

dotenv.config()

mongoose.connect(`mongodb+srv://carlosmiguelyulo813:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.4nsvfgb.mongodb.net/?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, 'Connection Error'))
db.on('open', () => console.log('Connected to MongoDB!'))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// User Schema
const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

// User Model 
const User = mongoose.model('User', userSchema)

app.post('/signup', (request, response) => {
    User.findOne({ username: request.body.username }, (error, result) => {
        
        if (result != null && result.username == request.body.username) {
            return response.send('Duplicate user found!')
 
        } else {
            
            if (request.body.username !== '' && request.body.password !== '') {
                
                let newUser = new User({
                    username: request.body.username,
                    password: request.body.password
                })

                newUser.save((error, savedUser) => {
                    if (error) {
                        return response.send(error)
                    }

                    return response.send('New user registered')
                })
            } else {
                return response.send('BOTH username and Password must be provided.')
            }
        }
    })
})

app.listen(port, () => console.log(`Server is running at ${port}`))